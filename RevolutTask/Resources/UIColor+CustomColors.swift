import UIKit

extension UIColor {
  static var accent: UIColor {
    return UIColor(red: 25 / 255, green: 113 / 255, blue: 202 / 255, alpha: 1)
  }
}
