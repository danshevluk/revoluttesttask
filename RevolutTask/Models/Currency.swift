import Foundation

struct Currency: Equatable {
  let identifier: String
}

extension Currency {
  var locale: Locale {
    let components: [String: String] = [NSLocale.Key.currencyCode.rawValue: self.identifier]
    let identifier = NSLocale.localeIdentifier(fromComponents: components)
    return Locale(identifier: identifier)
  }

  var localizedName: String? {
    return Locale.current.localizedString(forCurrencyCode: self.identifier)
  }
}
