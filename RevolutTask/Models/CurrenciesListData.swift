import Foundation
import SwiftyJSON

struct CurrenciesListData {
  let baseCurrency: Currency
  let rates: [CurrencyRate]

  /// List of currencies rates including base currency
  var fullRatesList: [CurrencyRate] {
    return [CurrencyRate(currency: baseCurrency, rate: 1)] + rates
  }

  /// Produces new CurrenciesListData object with provided base currency
  func withNewBaseCurrency(_ newBaseCurrency: Currency) -> CurrenciesListData {
    guard newBaseCurrency != baseCurrency else {
      return self
    }
    let fullRatesList = self.fullRatesList
    let nonBaseCurrenciesRates = fullRatesList.filter { $0.currency != newBaseCurrency }

    let rateForNewBase: Double
    if let currencyRate = fullRatesList.first(where: { $0.currency == newBaseCurrency }) {
      rateForNewBase = currencyRate.rate
    } else {
      rateForNewBase = 1
    }

    let updatedRates = nonBaseCurrenciesRates.map { currencyRate -> CurrencyRate in
      return CurrencyRate(currency: currencyRate.currency, rate: currencyRate.rate / rateForNewBase)
    }

    return CurrenciesListData(
      baseCurrency: newBaseCurrency,
      rates: updatedRates
    )
  }
}

// MARK: - JSONDecodable

extension CurrenciesListData: JSONDecodable {
  static func fromJSON(_ json: JSON) throws -> CurrenciesListData {
    guard let baseCurencyIdentifier = json["base"].string, let ratesDict = json["rates"].dictionary else {
      throw NetworkingError.invalidResponseData
    }

    let baseCurrency = Currency(identifier: baseCurencyIdentifier)
    let rates = ratesDict.reduce([], { (result, element) -> Array<CurrencyRate> in
      guard let rate = element.value.double else {
        return result
      }
      var mutableResult = result

      let currencyRate = CurrencyRate(currency: Currency(identifier: element.key), rate: rate)
      mutableResult.append(currencyRate)
      return mutableResult
    })

    return CurrenciesListData(
      baseCurrency: baseCurrency,
      rates: rates
    )
  }
}
