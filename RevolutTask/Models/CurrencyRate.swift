import Foundation

struct CurrencyRate {
  let currency: Currency
  let rate: Double
}

extension CurrencyRate {
  init(currencyIdentifier: String, rate: Double) {
    self.init(currency: Currency(identifier: currencyIdentifier), rate: rate)
  }
}
