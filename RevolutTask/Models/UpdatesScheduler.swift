import Foundation

class UpdatesScheduler {
  private var activeTimer: Timer?
  typealias ActionBlock = () -> Void

  private lazy var actionsCallbackQueue = DispatchQueue(
    label: "updatsScheduler.actionsCallbacks",
    qos: .userInitiated
  )

  func every(_ timeInterval: TimeInterval, perform action: @escaping ActionBlock) {
    let pendingTimer = Timer.scheduledTimer(
      withTimeInterval: timeInterval,
      repeats: true,
      block: { [weak self] _ in
        self?.actionsCallbackQueue.async {
          action()
        }
    })

    self.activeTimer?.invalidate()
    self.activeTimer = pendingTimer
  }

  deinit {
    self.activeTimer?.invalidate()
  }
}
