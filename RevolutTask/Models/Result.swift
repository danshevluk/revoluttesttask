import Foundation

enum Result<Value> {
  case success(Value)
  case error(Error)

  var isSuccess: Bool {
    switch self {
    case .success:
      return true
    case .error:
      return false
    }
  }

  var isError: Bool {
    return !isSuccess
  }

  var value: Value? {
    switch self {
    case .success(let value):
      return value
    case .error:
      return nil
    }
  }

  var error: Error? {
    switch self {
    case .success:
      return nil
    case .error(let error):
      return error
    }
  }
}
