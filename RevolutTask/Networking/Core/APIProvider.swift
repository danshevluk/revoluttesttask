import Moya

protocol APIProvider {
  associatedtype Target: TargetType
  typealias ProviderType = MoyaProvider<Target>
  typealias CompletionType<T> = (Result<T>) -> Void

  var provider: ProviderType { get }
  var processingQueue: DispatchQueue { get }
}

// MARK: - Request creation

extension APIProvider {

  func request<T: JSONDecodable>(_ token: Target, completion: @escaping CompletionType<T>) -> Cancellable {
    return self.provider.request(
      token,
      callbackQueue: self.processingQueue,
      completion: { result in
        switch result {
        case .success(let response):
          do {
            completion(.success(try T.extract(from: response)))
          } catch let error {
            completion(.error(error))
          }
        case .failure(let error):
          completion(.error(error))
        }
      }
    )
  }
}

// MARK: - Default plugins

extension APIProvider {
  static var defaultPlugins: [PluginType] {
    #if DEBUG
    var plugins: [PluginType] = [
      NetworkActivityPlugin(networkActivityClosure: ActivityIndicatorPlugin.shared.updateActivityIndicator)
    ]

    if CommandLine.arguments.contains("--log-networking") {
      plugins.append(NetworkLoggerPlugin(verbose: true, responseDataFormatter: jsonResponseDataFormatter))
    }

    return plugins
    #else
    return [
      NetworkActivityPlugin(networkActivityClosure: ActivityIndicatorPlugin.shared.updateActivityIndicator)
    ]
    #endif
  }
}

private func jsonResponseDataFormatter(_ data: Data) -> Data {
  do {
    let dataAsJSON = try JSONSerialization.jsonObject(with: data)
    let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
    return prettyData
  } catch {
    return data // fallback to original data if it can't be serialized.
  }
}
