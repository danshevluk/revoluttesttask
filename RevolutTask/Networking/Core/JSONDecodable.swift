import Foundation
import SwiftyJSON
import Moya

protocol JSONDecodable {
  static func fromJSON(_ json: JSON) throws -> Self
}

extension JSONDecodable {
  static func extract(from response: Response) throws -> Self {
    let responseJSON = JSON(try response.mapJSON())
    return try Self.fromJSON(responseJSON)
  }
}
