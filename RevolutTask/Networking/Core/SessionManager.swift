import Foundation

class SessionManager {
  let currenciesProvider: CurrenciesProvider

  init(isStubed: Bool = false) {
    currenciesProvider = CurrenciesProvider(isStubed: isStubed)
  }
}

extension SessionManager: CurrenciesProviderProxy { }
