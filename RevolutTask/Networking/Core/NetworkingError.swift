import Foundation

enum NetworkingError: Error {
  case invalidResponseData
}
