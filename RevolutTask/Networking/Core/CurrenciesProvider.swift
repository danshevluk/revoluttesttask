import Moya
import SwiftyJSON

// MARK: - Proxy protocol

protocol CurrenciesProviderProxy: CurrenciesRequestsProvider {
  var currenciesProvider: CurrenciesProvider { get }
}

extension CurrenciesProviderProxy {
  @discardableResult
  func loadCurrenciesList(for baseCurrency: Currency?,
                          completion: @escaping CurrenciesProvider.CompletionType<CurrenciesListData>) -> Cancellable {
    return self.currenciesProvider.loadCurrenciesList(for: baseCurrency, completion: completion)
  }
}

// MARK: - CurrenciesProvider

class CurrenciesProvider: APIProvider {
  typealias Target = CurrenciesTarget
  let provider: ProviderType
  lazy var processingQueue = DispatchQueue(label: "networking.currenciesProvider", qos: .default)

  init(isStubed: Bool = false) {
    let stubClosure: ProviderType.StubClosure
    if isStubed {
      stubClosure = MoyaProvider.immediatelyStub
    } else {
      stubClosure = MoyaProvider.neverStub
    }

    provider = ProviderType(stubClosure: stubClosure, plugins: CurrenciesProvider.defaultPlugins)
  }
}

// MARK: - Request methods

protocol CurrenciesRequestsProvider {
  @discardableResult
  func loadCurrenciesList(for baseCurrency: Currency?,
                          completion: @escaping CurrenciesProvider.CompletionType<CurrenciesListData>) -> Cancellable
}

extension CurrenciesProvider: CurrenciesRequestsProvider {
  @discardableResult
  func loadCurrenciesList(for baseCurrency: Currency?,
                          completion: @escaping CompletionType<CurrenciesListData>) -> Cancellable {
    return self.request(.loadRates(base: baseCurrency), completion: { result in
      DispatchQueue.main.async {
        completion(result)
      }
    })
  }
}
