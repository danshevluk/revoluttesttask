import Moya

@objc private class TestClass: NSObject { }

extension TargetType {

  /// Allows to load stubs for server rqeusts from .json files
  func stubbedResponse(_ filename: String) -> Data! {
    let bundle = Bundle(for: TestClass.self)
    let path = bundle.path(forResource: filename, ofType: "json")
    return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
  }
}
