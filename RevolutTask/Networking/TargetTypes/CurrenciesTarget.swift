import Moya

enum CurrenciesTarget {
  case loadRates(base: Currency?)
}

extension CurrenciesTarget: TargetType {
  private var baseURLString: String {
    switch self {
    case .loadRates:
      return "https://revolut.duckdns.org"
    }
  }

  var baseURL: URL {
    return URL(string: self.baseURLString)!
  }

  var path: String {
    switch self {
    case .loadRates:
      return "/latest"
    }
  }

  var method: Moya.Method {
    return .get
  }

  var task: Task {
    switch self {
    case .loadRates(let baseCurrency):
      let parameters: [String: Any]
      if let baseCurrency = baseCurrency {
        parameters = ["base": baseCurrency.identifier]
      } else {
        parameters = [:]
      }

      return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
  }

  var headers: [String: String]? {
    return nil
  }

  var sampleData: Data {
    switch self {
    case .loadRates:
      return stubbedResponse("CurrenciesListData")
    }
  }
}
