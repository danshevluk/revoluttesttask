import Moya

class ActivityIndicatorPlugin {
  static let shared = ActivityIndicatorPlugin()

  private var numberOfActiveRequests: Int = 0

  func updateActivityIndicator(_ changeType: NetworkActivityChangeType, _ targetType: TargetType) {
    switch changeType {
    case .began:
      DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
      }
      numberOfActiveRequests += 1
    case .ended:
      DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
      }
      numberOfActiveRequests = max(0, numberOfActiveRequests - 1)
    }

    DispatchQueue.main.async {
      let application = UIApplication.shared

      if self.numberOfActiveRequests > 0 && !application.isNetworkActivityIndicatorVisible {
        application.isNetworkActivityIndicatorVisible = true
      } else {
        application.isNetworkActivityIndicatorVisible = false
      }
    }
  }
}
