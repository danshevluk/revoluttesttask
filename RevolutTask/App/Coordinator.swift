import UIKit

enum CoordinatorResult<T> {
  case success(T)
  case error(Error)
}

typealias CoordinatorResultCallback<T> = (CoordinatorResult<T>) -> Void

protocol CoordinatorType {
  var isStarted: Bool { get }
  var rootViewController: UIViewController? { get }
  func start()
  var parent: CoordinatorType? { get }
  func addChild(coordinator: CoordinatorType)
  func removeChild(coordinator: CoordinatorType)
}

class Coordinator<ResultValue>: NSObject, CoordinatorType {
  typealias ResultCallback = (CoordinatorResult<ResultValue>) -> Void

  private let resultCallback: ResultCallback?
  private(set) var isStarted: Bool = false
  private(set) var childCoordinators: [CoordinatorType] = []
  var parent: CoordinatorType?

  var rootViewController: UIViewController? {
    return nil
  }

  init(resultCallback: ResultCallback? = nil) {
    self.resultCallback = resultCallback
  }

  func start() {
    guard !isStarted else { return }
    self.isStarted = true
  }

  func finish(result: CoordinatorResult<ResultValue>) {
    guard isStarted else { return }

    self.resultCallback?(result)
    self.parent?.removeChild(coordinator: self)
  }

  func addChild(coordinator: CoordinatorType) {
    guard indexOfChild(coordinator: coordinator) == nil else { return }

    childCoordinators.append(coordinator)
    coordinator.start()
  }

  func removeChild(coordinator: CoordinatorType) {
    guard let indexOfCoordinatorToRemove = indexOfChild(coordinator: coordinator) else { return }
    childCoordinators.remove(at: indexOfCoordinatorToRemove)
  }

  private func indexOfChild(coordinator: CoordinatorType) -> Int? {
    return childCoordinators.index(where: { $0.rootViewController == coordinator.rootViewController })
  }
}
