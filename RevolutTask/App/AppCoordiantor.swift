import UIKit

class AppCoordinator: Coordinator<Never> {
  let sessionManager: SessionManager
  let window: UIWindow

  init(sessionManager: SessionManager, window: UIWindow) {
    self.sessionManager = sessionManager
    self.window = window
    super.init(resultCallback: nil)
  }

  override var rootViewController: UIViewController? {
    return nil
  }

  override func addChild(coordinator: CoordinatorType) {
    super.addChild(coordinator: coordinator)

    guard let controller = coordinator.rootViewController else {
      fatalError("Root controller is not created")
    }

    let animated = window.rootViewController != nil
    setRootViewController(
      controller, forWindow: window,
      animated: animated,
      transition: .transitionCrossDissolve
    )
  }

  override func start() {
    super.start()

    presentCurrenciesCoordinator()
  }

  private func presentCurrenciesCoordinator() {
    let currenciesCoordinator = CurrenciesCoordinator(sessionManager: self.sessionManager) { [weak self] result in
      guard let self = self else { return }

      switch result {
      case .error(let error):
        print(error)
      default:
        break
      }
      // Reload currencies coordinator
      self.presentCurrenciesCoordinator()
    }

    self.addChild(coordinator: currenciesCoordinator)
  }
}

// MARK: - Root controller setup

extension AppCoordinator {
  private func setRootViewController(_ controller: UIViewController, forWindow window: UIWindow?, animated: Bool,
                                     transition: UIView.AnimationOptions) {
    guard let window = window else { return }

    guard animated else {
      window.rootViewController = controller
      window.makeKeyAndVisible()
      return
    }

    let duration: TimeInterval
    if transition == .transitionCrossDissolve {
      duration = 0.3
    } else {
      duration = 0.5
    }

    UIView.transition(with: window, duration: duration, options: transition, animations: {
      window.rootViewController = controller
      window.makeKeyAndVisible()
    })
  }
}
