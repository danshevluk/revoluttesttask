import UIKit

class CurrenciesListViewController: ViewController {
  let sessionManager: SessionManager
  var currenciesListData: CurrenciesListData?
  private var baseCurrencyValue: Double = 100

  private lazy var reloadingScheduler: UpdatesScheduler? = UpdatesScheduler()

  enum Constants {
    static let updatesFrequency: TimeInterval = 1
  }

  init(sessionManager: SessionManager) {
    self.sessionManager = sessionManager
    super.init()

    title = "Currencies"
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - View setup

  override func loadView() {
    let currenciesListView = CurrenciesListView()
    currenciesListView.dataSource = self
    currenciesListView.delegate = self
    self.view = currenciesListView
  }

  private var currenciesListView: CurrenciesListView {
    return self.view as! CurrenciesListView
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    setupActions()
  }
}

// MARK: - Actions setup

extension CurrenciesListViewController {
  private func setupActions() {
    restartUpdatesTimer()
  }

  private func restartUpdatesTimer() {
    reloadingScheduler = UpdatesScheduler()
    reloadingScheduler?.every(Constants.updatesFrequency) { [weak self] in
      self?.reloadCurrenciesList()
    }
  }
}

// MARK: - View actions delegate

extension CurrenciesListViewController: CurrenciesListViewActionsDelegate {
  func reloadCurrenciesList() {
    if currenciesListData == nil {
      DispatchQueue.main.async {
        self.currenciesListView.state = .loading
      }
    }

    sessionManager.loadCurrenciesList(for: currenciesListData?.baseCurrency) { result in
      switch result {
      case .success(let currenciesListData):
        self.currenciesListData = currenciesListData
        self.currenciesListView.state = .presentingContent
      case .error(let error):
        self.reloadingScheduler = nil
        self.currenciesListView.state = .loadingFailed(message: error.localizedDescription)
      }
    }
  }

  func didSelect(cell: CurrencyTableViewCell, at indexPath: IndexPath) {
    guard (indexPath.section, indexPath.row) != (0, 0) else { return }

    self.reloadingScheduler = nil
  }
}

// MARK: - CurrenciesListViewDataSource

extension CurrenciesListViewController: CurrenciesListViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let currenciesListData = currenciesListData else { return 0 }

    return currenciesListData.fullRatesList.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let currenciesListData = currenciesListData else {
      fatalError("Currencies list data is not loaded")
    }

    let cell: CurrencyTableViewCell = tableView.dequeueReusableCell(for: indexPath)
    let currencyRate = currenciesListData.fullRatesList[indexPath.row]
    configure(cell, with: currencyRate, baseCurrencyValue: baseCurrencyValue)
    cell.delegate = self
    return cell
  }

  private func configure(_ cell: CurrencyTableViewCell, with currencyRate: CurrencyRate, baseCurrencyValue: Double) {
    cell.nameLabel.text = currencyRate.currency.localizedName
    cell.identifierLabel.text = currencyRate.currency.identifier

    let numberFormatter = CurrencyValueTextField.numberFormatter
    let currencyPrice = (currencyRate.rate * baseCurrencyValue) as NSNumber
    cell.textField.text = numberFormatter.string(from: currencyPrice)
  }
}

extension CurrenciesListViewController: CurrencyTableViewCellActionsDelegate {
  func didStartEditingValue(for cell: CurrencyTableViewCell) {
    updateBaseCurrencyCell(selectedCell: cell)
  }

  private func updateBaseCurrencyCell(selectedCell: CurrencyTableViewCell) {
    guard let selectedCellIndexPath = currenciesListView.tableView.indexPath(for: selectedCell) else { return }
    guard let currenciesListData = currenciesListData else { return }

    self.reloadingScheduler = nil
    guard selectedCellIndexPath.row != 0 else {
      return
    }

    if let cellText = selectedCell.textField.text, let value = Double(cellText) {
      self.baseCurrencyValue = value
    } else {
      self.baseCurrencyValue = 0
    }
    let currency = currenciesListData.fullRatesList[selectedCellIndexPath.row].currency

    let tableView = self.currenciesListView.tableView
    selectedCell.updateViewState(isEditing: true)
    tableView.performBatchUpdates({
      self.currenciesListData = currenciesListData.withNewBaseCurrency(currency)
      tableView.moveRow(at: selectedCellIndexPath, to: IndexPath(row: 0, section: 0))
      tableView.setContentOffset(.zero, animated: true)
    }, completion: { _ in
      self.reloadVisibleCellsForNonBaseCurrencies(in: tableView)
      selectedCell.textField.becomeFirstResponder()
    })
  }


  private func reloadVisibleCellsForNonBaseCurrencies(in tableView: UITableView) {
    let rowsToUpdate = tableView.indexPathsForVisibleRows?.filter { ($0.section, $0.row) != (0, 0) }

    rowsToUpdate?.compactMap { indexPath -> (IndexPath, UITableViewCell)? in
      guard let cell = tableView.cellForRow(at: indexPath) else { return nil }

      return (indexPath, cell)
    }.forEach { (indexPath, cell) in
      guard let currencyCell = cell as? CurrencyTableViewCell else { return }
      guard let currencyRate = self.currenciesListData?.fullRatesList[indexPath.row] else { return }

      self.configure(currencyCell, with: currencyRate, baseCurrencyValue: self.baseCurrencyValue)
    }
  }

  func didChangeTextFieldValue(_ cell: CurrencyTableViewCell, value: Double) {
    self.baseCurrencyValue = value
    reloadVisibleCellsForNonBaseCurrencies(in: self.currenciesListView.tableView)
  }

  func didEndEditingValue(for cell: CurrencyTableViewCell) {
    restartUpdatesTimer()
  }
}
