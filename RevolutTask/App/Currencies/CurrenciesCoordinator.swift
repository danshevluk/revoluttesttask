import UIKit

class CurrenciesCoordinator: Coordinator<Void> {
  private let navigationController = UINavigationController()

  override var rootViewController: UIViewController? {
    return self.navigationController
  }

  let sessionManager: SessionManager

  init(sessionManager: SessionManager, resultCallback: ResultCallback?) {
    self.sessionManager = sessionManager
    super.init(resultCallback: resultCallback)
  }

  override func start() {
    super.start()

    presentCurrenciesListController()
  }

  private func presentCurrenciesListController() {
    let currenciesController = CurrenciesListViewController(sessionManager: sessionManager)
    navigationController.setViewControllers([currenciesController], animated: false)
  }
}

// MARK: - Binings

extension CurrenciesCoordinator {
  private func setupBindings(for currenciesListController: CurrenciesListViewController) {
    // Bindings for currencies list controller
  }
}
