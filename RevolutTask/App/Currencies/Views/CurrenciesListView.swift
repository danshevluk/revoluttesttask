import UIKit

typealias CurrenciesListViewDataSource = UITableViewDataSource

protocol CurrenciesListViewActionsDelegate: class {
  func reloadCurrenciesList()
  func didSelect(cell: CurrencyTableViewCell, at indexPath: IndexPath)
}

class CurrenciesListView: UIView {
  enum Constants {
    static let estimatedRowHeight: CGFloat = 90
  }

  private(set) lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.estimatedRowHeight = Constants.estimatedRowHeight
    tableView.rowHeight = UITableView.automaticDimension
    tableView.register(cellType: CurrencyTableViewCell.self)
    tableView.tableFooterView = UIView()
    tableView.delegate = self
    tableView.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    tableView.keyboardDismissMode = .onDrag
    tableView.contentInsetAdjustmentBehavior = .never
    return tableView
  }()

  weak var delegate: CurrenciesListViewActionsDelegate?

  var dataSource: CurrenciesListViewDataSource? {
    get {
      return tableView.dataSource
    }
    set {
      self.tableView.dataSource = newValue
    }
  }

  var state: State = .default {
    didSet {
      self.updateState(newState: self.state)
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupViews()
    updateState(newState: .default)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Views setup

  private func setupViews() {
    backgroundColor = UIColor.white
    addSubview(tableView)

    tableView.snp.makeConstraints { make in
      make.left.right.top.equalTo(self.safeAreaLayoutGuide)
      make.bottom.equalToSuperview()
    }
  }
}

// MARK: - State updates

extension CurrenciesListView {
  enum State {
    case loading
    case presentingContent
    case loadingFailed(message: String)

    static let `default`: State = .loading
  }

  private func updateState(newState: State) {
    switch newState {
    case .loading:
      displayLoadingIndicatorView()
    case .presentingContent:
      self.tableView.backgroundView = nil
    case .loadingFailed(let message):
      displayLoadingFailedView(message: message)
    }
    self.tableView.reloadData()
  }

  private func displayLoadingIndicatorView() {
    let loadingView = LoadingIndicatorView()
    loadingView.titleLabel.text = "Loading rates"
    self.tableView.backgroundView = loadingView
  }

  private func displayLoadingFailedView(message: String) {
    let loadingFailedView = LoadingFailedMessageView()
    loadingFailedView.errorMessageLabel.text = message
    loadingFailedView.retryButton.addTarget(self, action: #selector(reloadCurrenciesList), for: .touchUpInside)
    self.tableView.backgroundView = loadingFailedView
  }

  @objc private func reloadCurrenciesList() {
    self.delegate?.reloadCurrenciesList()
  }
}

// MARK: - UITableViewDelegate

extension CurrenciesListView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let cell = tableView.cellForRow(at: indexPath) as? CurrencyTableViewCell else { return }

    self.delegate?.didSelect(cell: cell, at: indexPath)
    cell.textField.becomeFirstResponder()
  }
}
