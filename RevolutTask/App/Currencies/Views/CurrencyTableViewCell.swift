import UIKit
import Reusable
import SnapKit

protocol CurrencyTableViewCellActionsDelegate: class {
  func didChangeTextFieldValue(_ cell: CurrencyTableViewCell, value: Double)
  func didStartEditingValue(for cell: CurrencyTableViewCell)
  func didEndEditingValue(for cell: CurrencyTableViewCell)
}

extension CurrencyTableViewCellActionsDelegate {
  func didChangeTextFieldValue(_ cell: CurrencyTableViewCell, value: Double) { }
  func didStartEditingValue(for cell: CurrencyTableViewCell) { }
  func didEndEditingValue(for cell: CurrencyTableViewCell) { }
}

class CurrencyTableViewCell: UITableViewCell, Reusable {
  let identifierLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 16, weight: .light)
    return label
  }()

  let nameLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    return label
  }()

  private let textFieldContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor.white
    view.layer.shadowColor = UIColor.accent.cgColor
    view.layer.shadowOpacity = 0.14
    view.layer.shadowOffset = CGSize(width: 0, height: 2)
    view.layer.shadowRadius = 3
    view.layer.cornerRadius = 12
    view.isHidden = true
    return view
  }()

  private(set) lazy var textField: CurrencyValueTextField = {
    let textField = CurrencyValueTextField()
    textField.delegate = self
    return textField
  }()

  weak var delegate: CurrencyTableViewCellActionsDelegate?

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    setupViews()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupViews() {
    selectionStyle = .none

    let labelsStackView = UIStackView(arrangedSubviews: [nameLabel, identifierLabel])
    labelsStackView.axis = .vertical
    labelsStackView.spacing = 10
    contentView.addSubview(labelsStackView)
    contentView.addSubview(textFieldContainerView)
    contentView.addSubview(textField)

    labelsStackView.snp.makeConstraints { make in
      make.top.bottom.equalToSuperview().inset(20)
      make.left.equalToSuperview().inset(20)
    }

    textFieldContainerView.snp.makeConstraints { make in
      make.right.equalToSuperview().inset(12)
      make.left.greaterThanOrEqualTo(labelsStackView.snp.right).offset(12)
      make.top.bottom.equalToSuperview().inset(12)
    }

    textField.snp.makeConstraints { make in
      make.left.right.equalTo(textFieldContainerView).inset(8)
      make.centerY.equalTo(textFieldContainerView)
      make.width.lessThanOrEqualToSuperview().multipliedBy(0.5)
    }
  }

  func updateViewState(isEditing: Bool) {
    if isEditing {
      nameLabel.textColor = UIColor.accent
      textFieldContainerView.isHidden = false
    } else {
      nameLabel.textColor = UIColor.black
      textFieldContainerView.isHidden = true
    }
  }
}

// MARK: - UITextFieldDelegate

extension CurrencyTableViewCell: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    updateViewState(isEditing: true)
    delegate?.didStartEditingValue(for: self)
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    updateViewState(isEditing: false)
    delegate?.didEndEditingValue(for: self)
  }

  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
    let text = (textField.text ?? "") as NSString
    let resultString = text.replacingCharacters(in: range, with: string)

    let numberFormatter = CurrencyValueTextField.numberFormatter
    if let currencyAmountValue = numberFormatter.number(from: resultString)?.doubleValue {
      self.delegate?.didChangeTextFieldValue(self, value: currencyAmountValue)
    } else if resultString.isEmpty {
      self.delegate?.didChangeTextFieldValue(self, value: 0)
    } else {
      return false
    }

    return true
  }

}
