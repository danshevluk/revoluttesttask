import UIKit

class CurrencyValueTextField: UITextField {
  override init(frame: CGRect) {
    super.init(frame: frame)

    setupViews()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupViews() {
    textAlignment = .right
    placeholder = "0"
    font = UIFont.systemFont(ofSize: 20)
    textColor = .black

    autocorrectionType = .no
    keyboardType = .decimalPad
    autocapitalizationType = .none
  }

  static var numberFormatter: NumberFormatter {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    numberFormatter.usesGroupingSeparator = false
    numberFormatter.maximumFractionDigits = 3
    numberFormatter.minimumFractionDigits = 0
    return numberFormatter
  }
}
