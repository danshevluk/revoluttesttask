import UIKit

class LoadingIndicatorView: UIView {
  let activityIndicatorView: UIActivityIndicatorView = {
    let activityIndicatorView = UIActivityIndicatorView(style: .gray)
    activityIndicatorView.startAnimating()
    return activityIndicatorView
  }()

  let titleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupViews()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupViews() {
    let stackView = UIStackView(arrangedSubviews: [activityIndicatorView, titleLabel])
    stackView.alignment = .center
    stackView.axis = .horizontal
    stackView.spacing = 12
    stackView.distribution = .fill

    addSubview(stackView)
    stackView.snp.makeConstraints { make in
      make.center.equalToSuperview()
    }
  }
}
