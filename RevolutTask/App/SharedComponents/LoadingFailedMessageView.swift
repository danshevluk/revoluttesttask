import UIKit

class LoadingFailedMessageView: UIView {
  let errorMessageLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.textColor = .black
    label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
    return label
  }()
  let retryButton: UIButton = {
    let button = UIButton()
    button.setTitle("Tap to retry", for: .normal)
    button.setTitleColor(UIColor.accent, for: .normal)
    button.setTitleColor(UIColor.accent.withAlphaComponent(0.6), for: .highlighted)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
    return button
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupViews()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupViews() {
    let stackView = UIStackView(arrangedSubviews: [errorMessageLabel, retryButton])
    stackView.alignment = .fill
    stackView.axis = .vertical
    stackView.spacing = 8
    stackView.distribution = .fill

    retryButton.snp.makeConstraints { make in
      make.height.equalTo(55)
    }


    addSubview(stackView)
    stackView.snp.makeConstraints { make in
      make.centerY.equalToSuperview()
      make.left.right.equalToSuperview().inset(20)
    }
  }
}
