import UIKit

class ViewController: UIViewController {
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .default
  }

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("Please don't use storyboards in this project")
  }
}
