import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)

  private lazy var appCoordinator: AppCoordinator? = {
    guard let window = self.window else { return nil }

    let sessionManager = SessionManager(isStubed: false)
    return AppCoordinator(sessionManager: sessionManager, window: window)
  }()

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    // Create and start application coordinator
    guard let appCoordinator = self.appCoordinator else {
      fatalError("Unable to create AppCoordinator")
    }
    appCoordinator.start()

    return true
  }
}
