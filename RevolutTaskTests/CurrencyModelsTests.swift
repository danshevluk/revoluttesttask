import XCTest
@testable import RevolutTask

class CurrencyModelsTests: XCTestCase {

  var listData: CurrenciesListData!

  override func setUp() {
    self.listData = CurrenciesListData(
      baseCurrency: Currency(identifier: "USD"),
      rates: [
        CurrencyRate(currencyIdentifier: "a", rate: 1),
        CurrencyRate(currencyIdentifier: "b", rate: 0.5),
        CurrencyRate(currencyIdentifier: "c", rate: 1.5)
      ]
    )
  }

  override func tearDown() {
    self.listData = nil
  }

  func testLocaleCurrencyIdentifier() {
    let identifier = "USD"
    let currency = Currency(identifier: identifier)
    let locale = currency.locale
    XCTAssertEqual(locale.currencyCode, identifier, "Currency codes are not matching")
  }

  func testLowercaseIdentifierCurrecyCreation() {
    let identifier = "USD"
    let currency = Currency(identifier: identifier.lowercased())
    XCTAssertEqual(currency.locale.currencyCode, identifier, "Currency codes are not matching")
  }

  func testCurrencyListDataBaseCurrencyUpdatesToSameBase() {
    let newListData = listData.withNewBaseCurrency(listData.baseCurrency)

    XCTAssertEqual(newListData.baseCurrency, listData.baseCurrency)
    XCTAssertEqual(newListData.rates.count, listData.rates.count)
  }

  func testCurrencyListDataBaseCurrencyUpdatesToOuterBase() {
    let notInludedCurrency = Currency(identifier: "ABC")
    let newListData = listData.withNewBaseCurrency(notInludedCurrency)

    XCTAssertEqual(newListData.baseCurrency, notInludedCurrency)
    XCTAssert(newListData.rates.contains(where: { $0.currency == listData.baseCurrency }))
  }

  func testCurrencyListDataFullRatesListContainsBase() {
    let fullRatesList = listData.fullRatesList
    XCTAssertEquals(fullRatesList.count, listData.rates.count + 1)
    XCTAssert(fullRatesList.contains(where: { $0.currency == listData.baseCurrency }))
  }
}
