import XCTest
@testable import RevolutTask

class SessionManagerTests: XCTestCase {
  var sessionManager: SessionManager!

  override func setUp() {
    sessionManager = SessionManager(isStubed: true)
  }

  override func tearDown() {
    sessionManager = nil
  }

  func testCurrenciesListLoading() {
    let expectation = XCTestExpectation(description: "Load currencies list")

    sessionManager.loadCurrenciesList(for: nil) { (result) in
      guard let currenciesList = result.value else {
        XCTAssertNotNil(result.value)
        return
      }


      XCTAssert(currenciesList.rates.count > 0, "Currencies list rates didn't load correctly")
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 10.0)
  }

  func testCurrenciesListLoadingCallsCallbackOnTheMainTherad() {
    let expectation = XCTestExpectation(description: "Load currencies list")

    sessionManager.loadCurrenciesList(for: nil) { (result) in
      XCTAssert(Thread.isMainThread, "Session manager methods could execute callbacks in the main thread")
      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 10.0)
  }
}
